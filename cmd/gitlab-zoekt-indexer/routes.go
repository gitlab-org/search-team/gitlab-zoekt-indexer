package main

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/correlation"
)

func (s *indexServer) router() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Heartbeat(s.getPath("/health")))
	r.Use(correlation.Middleware)

	r.Get(s.getPath("/status/{id}"), s.handleStatus())
	r.Post(s.getPath("/index"), s.handleIndex())
	r.Post(s.getPath("/truncate"), s.handleTruncate())
	r.Get(s.getPath("/metrics"), s.handleMetrics())

	return r
}

func (s *indexServer) getPath(path string) string {
	return s.pathPrefix + path
}
