package main

import (
	"context"
	"encoding/json"
	"io"
	"math"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/internal/projectpath"
)

const (
	pathPrefix = "/indexer"
)

type indexBuilderMock struct {
	indexDir string
}

func (b indexBuilderMock) indexRepository(ctx context.Context, req indexRequest) error {
	return nil
}

func (b indexBuilderMock) getIndexDir() string {
	return b.indexDir
}

func defaultIndexServer() indexServer {
	return indexServer{
		pathPrefix: pathPrefix,
	}
}

func TestCreateIndexDir(t *testing.T) {
	dirName := "tmp/test_create_index_dir_" + strconv.Itoa(rand.Intn(math.MaxInt))
	dir := filepath.Join(projectpath.Root, dirName)
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	s := indexServer{
		indexBuilder: indexBuilderMock{
			indexDir: dir,
		},
	}

	s.createIndexDir()
}

func TestInitMetrics(t *testing.T) {
	server := defaultIndexServer()

	server.initMetrics()

	require.NotNil(t, server.promRegistry)

	require.NotNil(t, server.metricsRequestsTotal)
}

func TestHandleHealthCheck(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/health", nil)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestHandleMetrics(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/metrics", nil)
	w := httptest.NewRecorder()

	server := defaultIndexServer()
	server.initMetrics()
	server.incrementRequestsTotal("GET", "/foo_bar", 200)
	server.handleMetrics()(w, req)

	resp := w.Result()

	body, _ := io.ReadAll(resp.Body)

	require.Equal(t, http.StatusOK, resp.StatusCode)
	require.Regexp(t, "gitlab_zoekt_indexer_requests_total.*foo_bar", string(body))
}

func TestHandleStatus(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/status/10", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	server := defaultIndexServer()
	server.indexBuilder = indexBuilderMock{
		indexDir: dir,
	}

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	type statusResponse struct {
		SHA     string
		Success bool
	}

	dec := json.NewDecoder(resp.Body)
	dec.DisallowUnknownFields()
	var parsedResponse statusResponse
	err := dec.Decode(&parsedResponse)

	require.NoError(t, err)

	expectedResponse := statusResponse{
		SHA:     "5f6ffd6461ba03e257d24eed4f1f33a7ee3c2784",
		Success: true,
	}

	if diff := cmp.Diff(expectedResponse, parsedResponse); diff != "" {
		t.Error(diff)
	}
}

func TestHandleStatusWhenErr(t *testing.T) {
	req := httptest.NewRequest("GET", "/indexer/status", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	server := defaultIndexServer()
	server.indexBuilder = indexBuilderMock{
		indexDir: dir,
	}

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusNotFound, resp.StatusCode)
}

func TestHandleTruncate(t *testing.T) {
	req := httptest.NewRequest("POST", "/indexer/truncate", nil)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/truncate_test")
	server := defaultIndexServer()
	server.indexBuilder = indexBuilderMock{
		indexDir: dir,
	}

	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	testFilePath := filepath.Join(dir, "test_file.txt")
	data := []byte("hello world")
	err := os.WriteFile(testFilePath, data, 0644)

	require.NoError(t, err)

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)

	if _, err := os.Stat(testFilePath); err == nil {
		t.Fatalf("file %v still exists", testFilePath)
	}
}

func TestHandleIndex(t *testing.T) {
	r := strings.NewReader(`{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/index_test")
	server := defaultIndexServer()
	server.indexBuilder = indexBuilderMock{
		indexDir: dir,
	}
	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	server.initMetrics()

	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestHandleIndexWhenErr(t *testing.T) {
	r := strings.NewReader(`{"nonExistingField": true}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/index_test")
	server := defaultIndexServer()
	server.indexBuilder = indexBuilderMock{
		indexDir: dir,
	}
	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	server.initMetrics()
	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
}

func TestHandleIndexWhenTimeoutErr(t *testing.T) {
	r := strings.NewReader(`{"GitalyConnectionInfo": {"Address": "unix:/praefect.socket", "Storage": "default", "Path": "7.git"}, "RepoId":7, "FileSizeLimit": 2097152, "Timeout": "1ns"}`)
	req := httptest.NewRequest("POST", "/indexer/index", r)
	w := httptest.NewRecorder()

	dir := filepath.Join(projectpath.Root, "tmp/index_test")
	server := defaultIndexServer()
	server.indexBuilder = defaultIndexBuilder{
		indexDir: dir,
	}
	server.createIndexDir()
	t.Cleanup(func() {
		os.RemoveAll(dir)
	})

	server.initMetrics()
	router := server.router()
	router.ServeHTTP(w, req)

	resp := w.Result()

	type response struct {
		Success bool
		Error   string
	}

	var parsedResp response
	err := json.NewDecoder(resp.Body).Decode(&parsedResp)

	require.NoError(t, err)
	require.Equal(t, http.StatusGatewayTimeout, resp.StatusCode)
	require.Equal(t, "context deadline exceeded", parsedResp.Error)
}
