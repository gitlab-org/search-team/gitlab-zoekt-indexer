package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/correlation"
	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/indexer"
)

type options struct {
	indexDir   string
	pathPrefix string
	listen     string
}

type indexServer struct {
	pathPrefix           string
	indexBuilder         indexBuilder
	promRegistry         *prometheus.Registry
	metricsRequestsTotal *prometheus.CounterVec
}

type gitalyConnectionInfo struct {
	Address string
	Token   string
	Storage string
	Path    string
}

type indexRequest struct {
	Timeout              string
	RepoID               uint32
	GitalyConnectionInfo *gitalyConnectionInfo
	FileSizeLimit        int
}

type indexBuilder interface {
	getIndexDir() string
	indexRepository(ctx context.Context, req indexRequest) error
}

type defaultIndexBuilder struct {
	indexDir string
}

func (b defaultIndexBuilder) getIndexDir() string {
	return b.indexDir
}

func (b defaultIndexBuilder) indexRepository(ctx context.Context, req indexRequest) error {
	if req.GitalyConnectionInfo == nil {
		return errors.New("gitalyConnectionInfo is not set")
	}

	if req.FileSizeLimit == 0 {
		return errors.New("fileSizeLimit is not set")
	}

	timeout, err := time.ParseDuration(req.Timeout)
	if err != nil {
		return fmt.Errorf("failed to parse Timeout: %v with error %v", req.Timeout, err)
	}

	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	idx := &indexer.Indexer{
		IndexDir:           b.indexDir,
		ProjectID:          req.RepoID,
		CorrelationID:      correlation.GetCorrelationIDFromContext(ctx),
		GitalyAddress:      req.GitalyConnectionInfo.Address,
		GitalyStorageName:  req.GitalyConnectionInfo.Storage,
		GitalyRelativePath: req.GitalyConnectionInfo.Path,
		LimitFileSize:      req.FileSizeLimit,
	}

	if err := idx.IndexRepository(ctx); err != nil {
		return err
	}

	return nil
}

func (s *indexServer) createIndexDir() {
	if err := os.MkdirAll(s.indexBuilder.getIndexDir(), 0o755); err != nil {
		log.Fatalf("createIndexDir %s: %v", s.indexBuilder.getIndexDir(), err)
	}
}

func (s *indexServer) handleStatus() http.HandlerFunc {
	route := "status"

	type response struct {
		Success bool
		SHA     string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		param := chi.URLParam(r, "id")
		repoID, err := strconv.ParseUint(param, 10, 32)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		idx := &indexer.Indexer{
			IndexDir:  s.indexBuilder.getIndexDir(),
			ProjectID: uint32(repoID),
		}

		currentSHA, err := idx.CurrentSHA()

		if err != nil {
			s.respondWithError(w, r, route, err)
			return
		}

		if err != nil {
			s.respondWithError(w, r, route, err)
			return
		}

		resp := response{
			Success: true,
			SHA:     currentSHA,
		}

		s.respondWith(w, r, route, resp)
	}
}

func (s *indexServer) handleMetrics() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		promhttp.HandlerFor(s.promRegistry, promhttp.HandlerOpts{Registry: s.promRegistry}).ServeHTTP(w, r)
	}
}

func (s *indexServer) decode(r *http.Request, v interface{}) error {
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	return dec.Decode(v)
}

func (s *indexServer) handleIndex() http.HandlerFunc {
	route := "index"

	type response struct {
		Success bool
	}

	parseRequest := func(r *http.Request) (indexRequest, error) {
		var req indexRequest
		err := s.decode(r, &req)

		if err != nil {
			return req, errors.New("json parser error")
		}

		return req, nil
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req, err := parseRequest(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = s.indexBuilder.indexRepository(r.Context(), req)
		if err != nil {
			s.respondWithError(w, r, route, err)
			return
		}

		resp := response{
			Success: true,
		}

		s.respondWith(w, r, route, resp)
	}
}

func (s *indexServer) handleTruncate() http.HandlerFunc {
	route := "truncate"

	type response struct {
		Success bool
	}

	emptyDirectory := func(dir string) error {
		files, err := os.ReadDir(dir)

		if err != nil {
			return err
		}

		for _, file := range files {
			filePath := filepath.Join(dir, file.Name())
			err := os.RemoveAll(filePath)
			if err != nil {
				return err
			}
		}

		return nil
	}

	return func(w http.ResponseWriter, r *http.Request) {
		err := emptyDirectory(s.indexBuilder.getIndexDir())

		if err != nil {
			err = fmt.Errorf("failed to empty indexDir: %v with error: %v", s.indexBuilder.getIndexDir(), err)

			s.respondWithError(w, r, route, err)
			return
		}

		resp := response{
			Success: true,
		}

		s.respondWith(w, r, route, resp)
	}
}

func (s *indexServer) respondWith(w http.ResponseWriter, r *http.Request, route string, data interface{}) {
	w.Header().Set("Content-Type", "application/json")

	if err := json.NewEncoder(w).Encode(data); err != nil {
		s.respondWithError(w, r, route, err)
		return
	}

	s.incrementRequestsTotal(r.Method, route, http.StatusOK)
}

func (s *indexServer) respondWithError(w http.ResponseWriter, r *http.Request, route string, err error) {
	type response struct {
		Success bool
		Error   string
	}

	responseCode := http.StatusInternalServerError

	if errors.Is(err, context.DeadlineExceeded) {
		responseCode = http.StatusGatewayTimeout
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseCode)

	resp := response{
		Success: false,
		Error:   err.Error(),
	}

	_ = json.NewEncoder(w).Encode(resp)

	s.incrementRequestsTotal(r.Method, route, responseCode)
}

func (s *indexServer) incrementRequestsTotal(method, route string, responseCode int) {
	s.metricsRequestsTotal.With(prometheus.Labels{"code": strconv.Itoa(responseCode), "method": method, "route": route}).Inc()
}

func (s *indexServer) initMetrics() {
	s.promRegistry = prometheus.NewRegistry()

	// Add go runtime metrics and process collectors.
	s.promRegistry.MustRegister(
		collectors.NewGoCollector(),
		collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}),
	)

	s.metricsRequestsTotal = promauto.With(s.promRegistry).NewCounterVec(
		prometheus.CounterOpts{
			Name: "gitlab_zoekt_indexer_requests_total",
			Help: "Total number of HTTP requests by status code, method, and route.",
		},
		[]string{"method", "route", "code"},
	)
}

func parseOptions() options {
	indexDir := flag.String("index_dir", "", "directory holding index shards.")
	pathPrefix := flag.String("path_prefix", "/indexer", "prefix for the routes")
	listen := flag.String("listen", ":6060", "listen on this address.")

	flag.Parse()

	if *indexDir == "" {
		log.Fatal("must set -index_dir")
	}

	return options{
		indexDir:   *indexDir,
		pathPrefix: *pathPrefix,
		listen:     *listen,
	}
}

func main() {
	opts := parseOptions()

	server := indexServer{
		pathPrefix: opts.pathPrefix,
		indexBuilder: defaultIndexBuilder{
			indexDir: opts.indexDir,
		},
	}

	server.startIndexingApi(opts.listen)
}

func (s *indexServer) startIndexingApi(listen string) {
	s.initMetrics()
	s.createIndexDir()

	httpServer := http.Server{
		Addr:    listen,
		Handler: s.router(),
	}

	log.Printf("Starting server on %s with '%s' as path prefix", listen, s.pathPrefix)

	if err := httpServer.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
