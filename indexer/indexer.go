package indexer

import (
	"context"
	"errors"

	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/gitaly"
	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/zoekt"
)

type Indexer struct {
	IndexDir           string
	ProjectID          uint32
	CorrelationID      string
	GitalyAddress      string
	GitalyToken        string
	GitalyStorageName  string
	GitalyRelativePath string
	gitalyClient       *gitaly.GitalyClient
	zoektClient        *zoekt.Client
	LimitFileSize      int
	indexingTimedOut   bool
}

func (i *Indexer) initClients() error {
	config := &gitaly.StorageConfig{
		Address:      i.GitalyAddress,
		Token:        i.GitalyToken,
		StorageName:  i.GitalyStorageName,
		RelativePath: i.GitalyRelativePath,
	}
	gitalyClient, err := gitaly.NewGitalyClient(config, i.CorrelationID, i.ProjectID, int64(i.LimitFileSize))

	if err != nil {
		return err
	}

	i.gitalyClient = gitalyClient

	gitalySHA, err := i.gitalyClient.GetCurrentSHA()

	if err != nil {
		return err
	}
	i.initZoektClient(gitalySHA)

	return nil
}

func (i *Indexer) initZoektClient(gitalySHA string) {
	branches := []zoekt.RepositoryBranch{
		{
			Name:    "HEAD",
			Version: gitalySHA,
		},
	}

	zoektOptions := &zoekt.Options{
		IndexDir: i.IndexDir,
		ID:       i.ProjectID,
		SizeMax:  i.LimitFileSize,
		Branches: branches,
	}

	i.zoektClient = zoekt.NewZoektClient(zoektOptions)
}

func (i *Indexer) CurrentSHA() (string, error) {
	i.initZoektClient("")

	zoektSHA, err := i.zoektClient.GetCurrentSHA()

	if err != nil {
		return "", err
	}

	return zoektSHA, nil
}

func (i *Indexer) IndexRepository(ctx context.Context) error {
	indexingCompleted := make(chan error)

	go func() {
		indexingCompleted <- i.indexRepository()
	}()

	select {
	case err := <-indexingCompleted:
		return err
	case <-ctx.Done():
		i.indexingTimedOut = true
		return ctx.Err()
	}
}

func (i *Indexer) indexRepository() error {
	if err := i.initClients(); err != nil {
		return err
	}

	gitalySHA, err := i.gitalyClient.GetCurrentSHA()

	if err != nil {
		return err
	}

	skipIndexing := i.zoektClient.IncrementalSkipIndexing()

	if skipIndexing {
		return nil
	}

	builder, err := i.zoektClient.NewBuilder()
	if err != nil {
		return err
	}

	putFunc := func(file *gitaly.File) error {
		if i.indexingTimedOut {
			return errors.New("indexing timeout")
		}

		content, e := file.BlobContent()
		if e != nil {
			return err
		}

		builder.MarkFileAsChangedOrRemoved(file.Path)
		if e = i.zoektClient.AddFile(builder, file.Path, content, file.Size, []string{"HEAD"}); e != nil {
			return e
		}

		return nil
	}

	delFunc := func(path string) error {
		if i.indexingTimedOut {
			return errors.New("indexing timeout")
		}

		builder.MarkFileAsChangedOrRemoved(path)
		return nil
	}

	i.gitalyClient.ToHash = gitalySHA

	err = i.gitalyClient.EachFileChange(putFunc, delFunc)

	if err != nil {
		return err
	}

	err = builder.Finish()

	if err != nil {
		return err
	}

	return nil
}
