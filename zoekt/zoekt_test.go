package zoekt_test

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/internal/projectpath"
	"gitlab.com/gitlab-org/search-team/gitlab-zoekt-indexer/zoekt"
)

var (
	ExistingID    uint32 = 10
	NonExistentID uint32 = 20
)

func TestGetCurrentSHA(t *testing.T) {
	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	client := zoekt.NewZoektClient(&zoekt.Options{
		IndexDir: dir,
		ID:       ExistingID,
	})

	result, err := client.GetCurrentSHA()

	require.NoError(t, err)

	expectedSHA := "5f6ffd6461ba03e257d24eed4f1f33a7ee3c2784"
	require.Equal(t, result, expectedSHA, "The two SHA should be the same.")

	client = zoekt.NewZoektClient(&zoekt.Options{
		IndexDir: dir,
		ID:       NonExistentID,
	})

	_, err = client.GetCurrentSHA()

	require.Error(t, err)
}
