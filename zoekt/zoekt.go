package zoekt

import (
	"fmt"

	"github.com/sourcegraph/zoekt"
	"github.com/sourcegraph/zoekt/build"
)

type RepositoryBranch struct {
	Name    string
	Version string
}

type Options struct {
	IndexDir   string
	ID         uint32
	RepoSource string
	SizeMax    int
	Branches   []RepositoryBranch
}

type Client struct {
	opts           *Options
	builderOptions *build.Options
}

func (rb *RepositoryBranch) castToZoekt() zoekt.RepositoryBranch {
	return zoekt.RepositoryBranch{
		Name:    rb.Name,
		Version: rb.Version,
	}
}

func NewZoektClient(opts *Options) *Client {
	return &Client{
		opts:           opts,
		builderOptions: defaultBuilderOptions(opts),
	}
}

func defaultBuilderOptions(opts *Options) *build.Options {
	var branches []zoekt.RepositoryBranch

	for _, b := range opts.Branches {
		branches = append(branches, b.castToZoekt())
	}

	buildOpts := build.Options{
		IndexDir: opts.IndexDir,
		SizeMax:  opts.SizeMax,
		RepositoryDescription: zoekt.Repository{
			ID:       opts.ID,
			Name:     fmt.Sprint(opts.ID),
			Source:   opts.RepoSource,
			Branches: branches,
		}}

	buildOpts.SetDefaults()
	return &buildOpts
}

func (c *Client) AddFile(builder *build.Builder, path string, content []byte, size int64, branches []string) error {
	if size > int64(c.builderOptions.SizeMax) && !c.builderOptions.IgnoreSizeMax(path) {
		if err := builder.Add(zoekt.Document{
			SkipReason: fmt.Sprintf("file size %d exceeds maximum size %d", size, c.builderOptions.SizeMax),
			Name:       path,
			Branches:   branches,
		}); err != nil {
			return err
		}

		return nil
	}

	if err := builder.Add(zoekt.Document{
		Name:     path,
		Content:  content,
		Branches: branches,
	}); err != nil {
		return fmt.Errorf("error adding document with name %s: %w", path, err)
	}

	return nil
}

func (c *Client) IncrementalSkipIndexing() bool {
	opts := c.builderOptions

	return opts.IncrementalSkipIndexing()
}

func (c *Client) NewBuilder() (*build.Builder, error) {
	builder, err := build.NewBuilder(*c.builderOptions)
	if err != nil {
		return nil, fmt.Errorf("build.NewBuilder: %w", err)
	}

	return builder, nil
}

func (c *Client) GetCurrentSHA() (string, error) {
	existingRepository, err := c.findRepositoryMetadata()

	if err != nil {
		return "", err
	}

	for _, branch := range existingRepository.Branches {
		if branch.Name == "HEAD" {
			return branch.Version, nil
		}
	}

	return "", nil
}

func (c *Client) findRepositoryMetadata() (*zoekt.Repository, error) {
	opts := c.builderOptions
	existingRepository, ok, err := opts.FindRepositoryMetadata()

	if err != nil {
		return nil, fmt.Errorf("failed to get repository metadata: %w", err)
	}

	if !ok {
		return nil, fmt.Errorf("no existing shards found for repository")
	}

	return existingRepository, nil
}
