# gitlab-zoekt-indexer

## Running tests

1. Install a [suitable docker client](https://handbook.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop)
1. Run the dependencies:
   ```
   docker-compose up
   ```
1. Run the tests:
   ```
   make test
   ```
